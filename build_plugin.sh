#!/bin/bash

if [[ $# -lt 1 ]] then
    printf "usage: build_plugin.sh <amxxpc path> [1 if debug enabled, 0 if disabled [path to copy gcljs.amxx to]]\nExpects the gcljs folder to be in the same folder as amxxpc.\n"
else
    debugargs="-d0"
    if [[ $# -ge 2 ]] then
        debugargs="-d$2"
        if [[ $2 -eq 3 ]] then
            debugargs="-d$2 DEBUG=1"
        fi
        
    fi

    pushd $1 >> /dev/null
    ./amxxpc ./gcljs/addons/amxmodx/scripting/gcljs.sma -w226- $debugargs -i./include -i./gcljs/addons/amxmodx/scripting/include -o./gcljs/addons/amxmodx/plugins/gcljs.amxx
    ./amxxpc ./gcljs/addons/amxmodx/scripting/gcljs.sma -w226- USE_SQL=1 $debugargs -i./include -i./gcljs/addons/amxmodx/scripting/include -o./gcljs/addons/amxmodx/plugins/gcljs_with_sql.amxx
    popd >> /dev/null

    if [[ $# -eq 3 ]] then
        cp -f ./addons/amxmodx/plugins/gcljs.amxx $3
        cp -f ./addons/amxmodx/plugins/gcljs_with_sql.amxx $3
    fi
fi
