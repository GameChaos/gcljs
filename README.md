# README #

This is a WIP jumpstatting plugin for CS 1.6. This plugin is mostly focused on being used on LAN, but it also works on servers. Some features like lj tops and database related things aren't in this plugin. Check the [wiki page](https://bitbucket.org/GameChaos/gcljs/wiki/Home) for documentation about the stats and cvars!

### How to install: ###

1. Make sure you have AMX Mod X and metamod installed. If you're using any KZ plugin packs, then you've already got both.
2. Download the latest version from [here](https://bitbucket.org/GameChaos/gcljs/downloads/?tab=tags)
3. Go to your `Half-Life\cstrike` folder and extract the contents of the zip file into that folder (excluding the LICENSE and README.md files).
4. Go to `Half-Life\cstrike\addons\amxmodx\configs` and open `plugins.ini` put `gcljs.amxx` at the end of that file.  
If you have any other LJ plugins installed, then you need to disable them.

## Changelog

#### 2.0.0dev

+ Added block distance and edge to ladderjump stats.
+ Added gcljs_enable_failstat_sounds
+ Added per-client options with `/gcljs`. To change the default client settings use `/gcljs_defaults`. For lan usage, you should use `/gcljs_defaults`.
+ Added jump potency stat, which is like gain, loss, sync and efficiency in one stat.
+ Added SQL database support for server owners. Use `gcljs_with_sql.amxx` instead of `gcljs.amxx` for sql. Lan users can keep using `gcljs.amxx`.
+ Added weapon to chat and console output.
* GCLJS now targets AMXX 1.10: https://www.amxmodx.org/downloads-new.php?branch=master
* Made jump validation more robust.
* Made hud strafe graph alignment consistent on linux (replaced `_` with `~`).
* Block distances are now shown with fractions.
* Speedometer now shows fractions (`/gcspeed`)
* Added jump replays (disabled for now, because the replays aren't used for anything; `gcljs_save_replays`)
* Fixed failstats not showing up if player gets teleported before hitting the ground.
* Fixed edge and block not showing up on bhop blocks.
* Fixed pre-25th and post-25th signature nightmare. Plugin now works with both versions with no configuration because the signatures are combined into one file.


#### 1.2.0

* Separated gcljs_show_hud_stats into 3 cvars: gcljs_show_hud_graph, gcljs_show_hud_strafe_stats, gcljs_show_hud_jump_stats.

#### 1.1.0

+ Added gcljs_hud_stats_vertical cvar.
+ Added gcljs_hud_jump_info_x cvar.
+ Added gcljs_clear_hud_and_beam_on_tp cvar.
* Fixed jump beams from different jumps overlapping and being inconsistent. Now only shows beams from only the last jump.
* Fixed jumpstats not being invalidated on teleports.

#### 1.0.0

+ Added strafestats to the hud.
+ Added duck graph to hud and console.
+ Added a whole jump total Loss stat.
* Fixed double tracking stats on the first frame of the jump.
* Instead of cutting off the end of the hud strafe graph, cut off the start of it to fit the whole graph onto the hud.

#### 0.2.2

* Fixed incorrect dead airtime and overlap on sideways jumps.
* Fixed an out of bounds bug.
* Made efficiency much more usable.

#### 0.2.1

* Fixed jump beam being always on, ignoring the setting of gcljs_show_jump_beam.

#### 0.2.0

* Renamed *deviation* to *veer*.
+ Added veer beam (/veerbeam) which shows you how much you veered left or right of the x or y axis.
+ Added jump beam (/jumpbeam). Green means you gained speed, yellow means your speed stayed the same, red means you lost speed and purple means you ducked.
+ Added an options menu (/gcljsoptions) and a command to save options (/gcsavesettings, command name will be changed later).
+ Added sideways and backwards stat support.
+ Added jump direction to console stats.
+ Added FOG (frames on ground) to speed panel and console stats.
+ Added stamina stats to console stats.
+ Added landing edge to console stats.
+ Added XJ distance to the HUD.
* Messed with the order of stats on the hud and in the console. They should now be roughly in the order of importance.
* Moved mouse graph down a tiny bit.
* Fixed some failstat bugs.

#### 0.1.0

You'll need to delete gcljs.cfg in `cstrike\addons\amxmodx\configs` for the plugin to auto generate a new cfg file with the new cvars!

+ Added CJ, DCJ, MCJ, SCJ, WJ, LDJ, BH, DuckBH and SBJ stats.
+ Added XJ distance to console stats. This should match the way that XJ measures jumpstats.
+ Added prespeed to speed panel.
+ Added cvar for enabling/disabling speed panel.
+ Added cvar for HUD y offset.
+ Stats now get sent to spectators as well.
* Fixed many bugs.
* Swapped Edge and OL/DA in the chat and hud.

#### 0.0.3

+ Added jumpoff angle.
+ Added airpath stat.
+ Added strafe efficiency.
+ Added cvar for /gcspeed
* Changed HUD formatting.
* Fixed slightly wrong distances.
